// g++ src/gen-keys.cpp -lntl -lgmp -lm -Wall -Werror -o gen -O3
#include <vector>
#include <iostream>
#include <getopt.h>
#include <sstream>
#include <chrono>
#include <cmath>
#include <cinttypes>

#include "../include/Polynomial.h"
#include "../include/DiscreteLaplacian.h"

using namespace std;
using namespace std::chrono;

const static float SCALE_DEFAULT = 0.5f;
const static uint64_t PLAIN_MODULUS_BITS_DEFAULT = 16;
const static unsigned int LOG2_3 = 2;
const static unsigned int ITERATIONS = 5;


unsigned int ctext_modulus_size(const unsigned int log_t, const size_t num_users){
  unsigned int log_num_users = 1;
  while(((unsigned int)1 << log_num_users) < num_users){
    log_num_users++;
  }
  cout << "log_num_users," << log_num_users << endl;
  unsigned int q_bitsize = (log_t) + log_num_users + LOG2_3;
  return q_bitsize;
}


int main(int argc, char ** argv){
  unsigned int num_aggregations = 100;
	uint64_t t = 1;
	unsigned int log_t = PLAIN_MODULUS_BITS_DEFAULT;
	unsigned int num_users = 100;
	
	srand(time(NULL));
	
	int c;
	
	while ((c = getopt(argc, argv, "t:i:n:")) != -1){
		switch(c){
			case 't':{
				log_t = atoi(optarg);
				break;
			}
			case 'i':{
				num_aggregations = atoi(optarg);
				break;
			}
			case 'n':{
				num_users = atoi(optarg);
				break;
			}
			default:{
				cout << "ERROR: unrecognized input: " << c << endl;
				return 0;
			}
		}
	}
	
	assert(num_users);
	unsigned int log_num_users = ceil(log2(num_users));
	unsigned int practical_plaintext_space = log_t + log_num_users;
	assert(practical_plaintext_space < 64);
	t <<= practical_plaintext_space;
	uint64_t clear_space = 1;
	clear_space <<= log_t;
	
	vector<double> sample_secrets, derive_public, mult_times;
	sample_secrets.reserve(num_aggregations);
	derive_public.reserve(num_aggregations);
	mult_times.reserve(num_aggregations);
	
	unsigned int ciphertext_mod_bits = ctext_modulus_size(log_t, num_users);
	
	Parameters parms(ciphertext_mod_bits);
	unsigned int N = parms.poly_mod_degree();
	
	unsigned int num_polynomials_needed = (num_aggregations / N);
	if(num_aggregations % N){
	  num_polynomials_needed++;
	}
	
	Polynomial secret_key(&parms), public_key(&parms), tmp(&parms);
	
	
	double d;
	steady_clock::time_point start, end;
	DiscreteLaplacian dl;
	
	for(unsigned int i = 0; i < ITERATIONS; i++){
	
	  //Time to sample all secret keys
	  start = steady_clock::now();
	  for(unsigned int j = 0; j < num_users; j++){
	    dl.refresh();
	    secret_key.error(dl);
	  }  
	  end = steady_clock::now();
	  d = duration_cast<std::chrono::nanoseconds>(end-start).count();
	  sample_secrets.push_back(d);
	  
	  //Time to derive public keys from timestamp, for all needed timestamps
	  vector<uint64_t> ts(num_polynomials_needed);
	  for(uint64_t & u : ts){
	    u = rand();
	    u <<= 32;
	    u |= rand();
	  } 
	  
	  start = steady_clock::now();
	  for(size_t j = 0; j < num_polynomials_needed; j++){
	    dl.refresh(ts[j]);
	    public_key.uniform(dl);
	  }  
	  end = steady_clock::now();
	  end = steady_clock::now();
	  d = duration_cast<std::chrono::nanoseconds>(end-start).count();
	  derive_public.push_back(d);
	  
	  //Time to precompute all a user's A*s terms
	  start = steady_clock::now();
	  for(size_t j = 0; j < num_polynomials_needed; j++){
	    tmp = public_key*secret_key;
	  }  
	  end = steady_clock::now();
	  end = steady_clock::now();
	  d = duration_cast<std::chrono::nanoseconds>(end-start).count();
	  mult_times.push_back(d);
	}
	
	double q_actual = 0;
	for(size_t i = 0; i < parms.moduli_count(); i++){
	  q_actual += log2(parms.moduli(i));
	}
	
	cout << "log_t," << log_t << endl;
	cout << "num_users," << num_users << endl;
	cout << "q_bits_requested," << ciphertext_mod_bits << endl;
	cout << "q_bits_actual," << q_actual << endl;
	cout << "ctext_moduli," << parms.moduli_count() << endl;
	cout << "poly_mod_degree," << parms.poly_mod_degree() << endl;
	cout << "iterations," << ITERATIONS << endl;
	cout << "num_polynomials_needed," << num_polynomials_needed << endl;
	
	cout << "secret,";
	for(const double d : sample_secrets){
	  cout << d << ',';
	}
	cout << endl;
	
	cout << "public,";
	for(const double d : derive_public){
	  cout << d << ',';
	}
	cout << endl;
	
	cout << "multiply,";
	for(const double d : mult_times){
	  cout << d << ',';
	}
	cout << endl;
	
	
	
	return 0;
}
