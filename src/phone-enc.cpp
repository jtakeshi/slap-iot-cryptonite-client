// ~/Repositories/android-ndk/android-ndk-r23b/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android30-clang++ -pie src/phone-enc.cpp -O3 -o phone-enc -std=c++17 -static-libstdc++
#include <vector>
#include <iostream>
#include <getopt.h>
#include <sstream>

#include "../include/Aggregator_Coeff.h"

using namespace std;

const static float SCALE_DEFAULT = 0.5f;
const static uint64_t PLAIN_MODULUS_BITS_DEFAULT = 16;

int main(int argc, char ** argv){

	bool do_noise = false;
	int num, den;
	float scale = SCALE_DEFAULT;
	float_to_frac(scale, num, den);
	unsigned int num_aggregations = 100;
	uint64_t t = 1;
	unsigned int log_t = PLAIN_MODULUS_BITS_DEFAULT;
	unsigned int num_users = 100;

	vector<uint64_t> moduli, keys, delta_mod_q;

	int c;
	
	while ((c = getopt(argc, argv, "t:i:n:m:")) != -1){
		switch(c){
			case 't':{
				log_t = atoi(optarg);
				break;
			}
			case 'i':{
				num_aggregations = atoi(optarg);
				break;
			}
			case 'n':{
				num_users = atoi(optarg);
				break;
			}
			case 'm':{
				stringstream num_stream(optarg);
				uint64_t m;
				num_stream >> m;
				moduli.push_back(m);
				break;
			}
			default:{
				cout << "ERROR: unrecognized input: " << c << endl;
				return 0;
			}
		}
	}

	assert(num_users);
	unsigned int log_num_users = ceil(log2(num_users));
	unsigned int practical_plaintext_space = log_t + log_num_users;
	assert(practical_plaintext_space < 64);
	t <<= practical_plaintext_space;
	uint64_t clear_space = 1;
	clear_space <<= log_t;

	vector<double> noise_times, enc_times;
	if(do_noise){
		noise_times.reserve(num_aggregations);
	}
	enc_times.reserve(num_aggregations);

	double key_gen_time;

	double ctext_bits = generate_params(t, num_aggregations, num_users,
		moduli, keys, delta_mod_q, key_gen_time);
	DiscreteLaplacian dl(scale);

	for(unsigned int i = 0; i < num_aggregations; i++){
		//User input is in cleartext space
		uint64_t user_input = dl.uniform_64(clear_space);
		double noise_time, enc_time;

		auto ret = enc_noclass(user_input, 
			i, 
	    keys, do_noise, 
	    noise_time, enc_time, moduli, 
	    delta_mod_q,
	    dl, t,
	    num, den);

		if(do_noise){
			noise_times.push_back(noise_time);
		}
		enc_times.push_back(enc_time);

	}

	cout << "users," << num_users << endl;
	cout << "cleartext_bits," << log_t << endl;
	cout << "plaintext_bits," << practical_plaintext_space << endl;
	cout << "ctext_bits," << ctext_bits << endl;
	cout << "key_gen," << key_gen_time << endl;
	if(do_noise){
		cout << "noise,";
		for(const double d : noise_times){
			cout << d << ',';
		}
		cout << endl;
	}
	cout << "enc,";
	for(const double d : enc_times){
		cout << d << ',';
	}
	cout << endl;


	return 0;
}
