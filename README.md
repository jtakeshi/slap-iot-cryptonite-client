# Android Running Workflow
Kudos to https://nickdesaulniers.github.io/blog/2016/07/01/android-cli/

1. Download https://developer.android.com/ndk/downloads/index.html
(`android-ndk-r23-linux.zip` in my case)
```
  android {
      ndkVersion "23.0.7599858"
  }
```
2. Install Android adb and fastboot (Arch Linux: `pacman -S android-tools`)
3. Identify architecture: `arm64`
  - Google Pixel 4a ISA: `ARMv8`
  - Samsung Galaxy Note 8 ISA: `ARMv8-A`
4. This means use prebuilt toolchain with prefix:
`./android-ndk-r23/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android*`
5. Let's try a hello world problem
```
$ cat hello_world.c
#include <stdio.h>
int main () {
  puts("hello world");
}
```
6. Compile (Since Android Lollipop, Android has required that executables be linked as position independent (`-pie`) to help provide ASLR). Chooose Android version (API level) based on phone (see https://source.android.com/setup/start/build-numbers ). Mine happens to be 28.
`./android-ndk-r23/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android28-clang -pie hello_world.c -o hello_world`
7. Connect your phone, enable remote debugging, and accept the prompt for remote debugging
8. `adb push hello_world /data/local/tmp/.`
9. `adb shell "./data/local/tmp/hello_world"`

# Compiling and Running the Driver
I have my project structured as follows:
```text
├── include
│   ├── Aggregator_Coeff.h
│   ├── Aggregator_NTL.h
│   ├── Aggregator_RNS.h
│   ├── CKKS_Aggregator.h
│   ├── CKKS_Encoder.h
│   ├── DiscreteLaplacian.h
│   ├── embedding.h
│   ├── numbertheory.h
│   ├── Polynomial.h
│   ├── polyutils.h
│   └── scheme.h
└── src
    ├── aggregation.cpp
    ├── compare.cpp
    ├── embedding.cpp
    ├── iot_client.cpp
    ├── iot_server.cpp
    ├── phone-enc.cpp
    └── rns_embedding.cpp
```

Compilation requires statically linking libstdc++ which increases the binary size. Alternatively,
you can also push `libc++_shared.so` in the same directory as the program.

Connect your phone then run the following commands:
```shell
# Compile
./android-ndk-r23/toolchains/llvm/prebuilt/linux-x86_64/bin/aarch64-linux-android28-clang++ \
    -pie -std=c++17 -static-libstdc++ \
    -I include/ \
    src/phone-enc.cpp \
    -o phone_enc
# Push (NB the exact specification of paths is important, otherwise permissioning errors may occur.)
adb push phone_enc /data/local/tmp/.
# Run
adb shell "./data/local/tmp/phone_enc"
```
